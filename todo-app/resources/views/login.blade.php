@extends('layout.master')
@section('title','Login')
@section('body')
    <div class="todo_app">
        <div class="app_title">
            Todo App
        </div>
        <h1>@yield('title')</h1>
        <div class="form-login">
            <form action="" method="POST">
                @csrf
                <div class="email">Email: <input type="email" id="email" name="email" style="height: 25px; width: 200px"></div>
                <br><br>
                <div class="password">Password: <input type="password" id="password" name="password" style="height: 25px; width: 200px"></div>
                <br><br>
                @if(session('notification'))
                    <div class="alert alert-warning" role="alert" style="color:green;">
                        {{session('notification')}}
                    </div>
                @endif
                @if(session('notifications'))
                    <div class="alert alert-warning" role="alert" style="color:red;">
                        {{session('notifications')}}
                    </div>
                @endif
                <div class="submit">
                    <button type="submit" id="btn-login">Login</button>
                </div>
                <br>
            </form>
            <a href="./register" style="color: #000">Or create an account</a>
        </div>
    </div>
@endsection