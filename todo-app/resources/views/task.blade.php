@extends('layout.master')
@section('title','Task')
@section('body')
    <div class="todo_app">
        <div class="app_title">
            Todo App
        </div>
        <div class="welcome_user">Welcome {{Auth::user()->name}}
        </div>
        <div class="logout">
            <a href="./logout" style="text-decoration:none; color:brown">Log out</a><br>
        </div>
        <h1>@yield('title')</h1>
        <div class="task">
            Task's name <input type="text" name="task_name" value="{{ $task_choose->task_name }}" disabled> <br><br>
            Description <textarea type="text" name="description" disabled>{{ $task_choose->description }}</textarea> <br><br> 
            <label for="status">Status</label>
            <select id="status" name="status" disabled>
                <option value="complete" {{$task_choose->status=="complete" ? 'selected' : ''}}>Complete</option>
                <option value="in-progess" {{$task_choose->status=="in-progess" ? 'selected' : ''}}>In progess</option>
                <option value="pending" {{$task_choose->status=="pending" ? 'selected' : ''}}>Pending</option>
            </select>
            <br><br>
            Start day <input type="text" name="" value="{{ $task_choose->created_at }}" disabled>
            <br><br>
        </div>
        <a href="./home" style="text-decoration: none; color:brown">Home</a>
    </div>
@endsection