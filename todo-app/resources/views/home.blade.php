@extends('layout.master')
@section('title','Home')
@section('body')
    <div class="todo_app">
        <div class="app_title">
            Todo App
        </div>
        <div class="welcome_user">Welcome {{Auth::user()->name}}
        </div>
        <div class="logout">
            <a href="./logout" style="text-decoration:none; color:brown">Log out</a><br>
        </div>
        <h1>Your Task</h1>
        <div class="list-task" style="height:200px;">
            @if ($user_tasks->isEmpty())
                <p>You dont' have any task</p>
            @else
            <table style="width:100%; text-align:center">
                <tr>
                    <th>Serial</th>
                    <th>Task</th>
                    <th>Status</th>
                    <th>Manage Task</th>
                </tr>
                @foreach ($user_tasks as $task)
                <tr>
                    <td>{{ $task_number++ }}</td>
                    <td><p><a href="{{URL::to('/home/task/'.$task->id)}}" style="text-decoration:none; color:black;">{{ $task->task_name }}</p></a></td>
                    <td><p style="color: {{$task->status == "complete" ? 'green' : ($task->status == "in-progess" ? 'DarkOrange' : 'DarkGray')}}">{{ $task->status }}</p></td>
                    <td><a href="{{URL::to('/home/edit_task/'.$task->id)}}" style="text-decoration:none; color:blue">Edit </a>|
                        <a href="{{URL::to('/home/delete_task/'.$task->id)}}" onclick="return confirmDelete()" style="text-decoration:none; color:red">Delete </a></td>
                </tr>
                @endforeach
              </table>
            @endif
        </div>
        <span>{!! $user_tasks->withQueryString()->links('pagination::bootstrap-5') !!}</span>
        @if(session('success'))
            <div class="success" style="color:green">
                {{session('success')}}
            </div>
        @endif
        <form method="POST" action="{{ URL::to('/home') }}" style="margin-top:20px">
            @csrf
            Task's name <input type="text" name="task_name"> <br><br>
            Description <textarea type="text" name="description"></textarea> <br><br> 
            <label for="status">Status</label>
            <select id="status" name="status">
                <option value="complete">Complete</option>
                <option value="in-progess">In progess</option>
                <option value="pending">Pending</option>
            </select>
            <br><br>
            <button type="submit">Add New Task</button>
        </form>
        @if ($errors->any())
            <div class="erros" style="color:red">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('notifications'))
            <div class="alert alert-warning" role="alert" style="color:red;">
                {{session('notifications')}}
            </div>
        @endif
    </div>
@endsection
    
