@extends('layout.master')
@section('title','Edit')
@section('body')
    <div class="todo_app">
        <div class="app_title">
            Todo App
        </div>
        <div class="welcome_user">Welcome {{Auth::user()->name}}
        </div>
        <div class="logout">
            <a href="./logout" style="text-decoration:none; color:brown">Log out</a><br>
        </div>
        <h1>@yield('title')</h1>
        <form method="POST" action="{{URL::to('/home/update_task/'.$task_edit->id)}}" style="margin-top:20px">
            @csrf
            Task's name <input type="text" name="task_name" value="{{ $task_edit->task_name }}"> <br><br>
            Description <textarea type="text" name="description">{{ $task_edit->description }}</textarea> <br><br> 
            <label for="status">Status</label>
            <select id="status" name="status">
                <option value="complete" {{$task_edit->status=="complete" ? 'selected' : ''}}>Complete</option>
                <option value="in-progess" {{$task_edit->status=="in-progess" ? 'selected' : ''}}>In progess</option>
                <option value="pending" {{$task_edit->status=="pending" ? 'selected' : ''}}>Pending</option>
            </select>
            <br><br>
            <button type="submit">Edit Task</button>
        </form>
        <br>
        <a href="./home" style="text-decoration: none; color:brown">Home</a>
        @if ($errors->any())
            <div class="erros" style="color:red">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endsection