@extends('layout.master')
@section('title','Register')
@section('body')
    <div class="todo_app">
        <div class="app_title">
            Todo App
        </div>
        <h1>@yield('title')</h1>
        <div class="form-login" style="margin-top: 40px">
            <form action="" method="POST">
                @csrf
                <div class="name">Name: <input type="text" id="name" name="name" style="height: 25px; width: 200px" required></div>
                <br>
                <div class="email">Email: <input type="email" id="email" name="email" style="height: 25px; width: 200px" required></div>
                <br>
                <div class="password">Password: <input type="password" id="password" name="password" style="height: 25px; width: 200px" required></div>
                <br>
                <div class="password_confirm">
                    Confirm Password: 
                    <input type="password" id="password_confirm" name="password_confirm" style="height: 25px; width: 200px" required>
                </div>
                <br>
                @if(session('notification'))
                    <div class="alert alert-warning" role="alert" style="color:red;">
                        {{session('notification')}}
                    </div>
                @endif
                @if(session('notifications'))
                    <div class="alert alert-warning" role="alert" style="color:red;">
                        {{session('notifications')}}
                    </div>
                @endif
                <div class="submit">
                    <button type="submit" id="btn-signup">Sign up</button>
                </div>
            </form>
                <br>
            <a href="./" style="color: #000">Or Login</a>
        </div>
    </div>
@endsection