<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    public function index() {
        $task_number=1;
        $user_id = Auth::user()->id;
        $user_tasks = DB::table('tbl_task')->join('users','tbl_task.user_id','=','users.id')
                        ->where('users.id', $user_id)
                        ->select('tbl_task.id','task_name','status','description')
                        ->paginate(4);
        return view('home', compact('user_tasks','task_number'));
    }
    public function add(Request $request) {
        $user_id = Auth::user()->id;
        $user_tasks = DB::table('tbl_task')->join('users','tbl_task.user_id','=','users.id')
                        ->where('users.id', $user_id)
                        ->select('tbl_task.id','task_name','status','description')
                        ->get();
        $data = $request->validate([
            'task_name'=>'required|min:3|max:20|unique:tbl_task|regex:/^[A-Za-z][A-Za-z\s]*$/',
            'description'=>'required',
            'status'=>'required',
        ]);
        foreach ($user_tasks as $user_task) {
            if($user_task->task_name == $data['task_name']){
                return back()->with('notifications', 'Please give the task another name');
            }
        }
        $task = new Task();
        $task->user_id = Auth::user()->id;
        $task->task_name = $data['task_name'];
        $task->description = $data['description'];
        $task->status = $data['status'];
        $task->save();
        return redirect('/home')->with('success','Task has been added successfully');
    }
    public function edit_task($id) {
        $task_edit = Task::find($id);
        return view('edit', compact('task_edit'));
    }
    public function update_task($id) {
        $data= request()->validate([
            'task_name'=>'required|min:3|max:10|regex:/^[A-Za-z][A-Za-z\s]*$/',
            'description'=>'required',
            'status'=>'required',
        ]);
        $task = Task::find($id);
        $task->task_name = $data['task_name'];
        $task->description = $data['description'];
        $task->status = $data['status'];
        $task->save();
        return redirect('/home')->with('success','Task has been updated successful');
    }
    public function delete_task($id) {  
        $task = Task::find($id);
        $task->delete();
        return redirect('/home')->with('success','Task has been deleted successful');
    }
    public function task($id) {
        $task_choose = Task::find($id);
        return view('task', compact('task_choose'));
    }
}
