<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index() {
        return view('register');
    }

    public function register(Request $request) {
        $users = User::all();
        if($request->password != $request->password_confirm) {
            return back()->with('notifications', 'Confirm password does not match');
        }
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return back()->with('notifications', 'Email is invalid');
        }
        if(strlen($request->password) < 6){
            return back()->with('notifications', 'Password must be at least 6 characters');
        }
        if(!preg_match('/^[A-Za-z0-9]+$/', $request->password)){
            return back()->with('notifications', 'Passwords must only include uppercase, lowercase letters and numbers');
        }
        $data = [
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>$request->password
        ];
        foreach ($users as $user1) {
            if($user1->email == $data['email']) {
                return back()->with('notifications', 'This Email is already exist');
            }
        }
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->save();
        return redirect('./')->with('notification', 'Register Success! Please login');
    }
}
