<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index() {
        return view('login');
    }
    public function checkLogin(Request $request) {
        $credentials = [
            'email'=> $request->email,
            'password'=> $request->password,
        ];

        $remember = $request->remember;
        if( Auth::attempt($credentials, $remember)) {
            return redirect('/home');
        } else{
            return back()->with('notifications','Email or password is incorrect!');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('./');
    }
}
