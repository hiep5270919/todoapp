<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('')->group(function(){
    Route::get('/',[Front\LoginController::class, 'index']);
    Route::post('/',[Front\LoginController::class, 'checkLogin']);
    Route::get('/logout',[Front\LoginController::class, 'logout']);
});

Route::prefix('register')->group(function(){
    Route::get('/',[Front\RegisterController::class, 'index']);
    Route::post('/',[Front\RegisterController::class, 'register']);
});

Route::prefix('home')->group(function(){
    Route::get('/',[Front\HomeController::class, 'index']);
    Route::post('/',[Front\HomeController::class, 'add']);
    Route::get('/edit_task/{id}',[Front\HomeController::class, 'edit_task']);
    Route::post('/update_task/{id}',[Front\HomeController::class, 'update_task']);
    Route::get('/delete_task/{id}',[Front\HomeController::class, 'delete_task']);
    Route::get('/task/{id}',[Front\HomeController::class, 'task']);
});


